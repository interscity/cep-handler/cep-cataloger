# README

The CEP-Cataloger microservice is responsible for registering all event types in the system. It provides an API for developers to register new event types and web-hooks (URIs which are called by CEP-Cataloger when specified events are detected). CEP-Cataloger uses a DBMS to store all event type metadata to be used when event types are relocated in CEP-Worker instances. The DBMS must be scalable by replication in order to ensure availability, since CEP-Worker instances will query event types metadata frequently.



